import i18n from 'i18next'

i18n.init({
  debug: false,
  fallbackLng: 'ru',
  resources: {
    ru: {
      translation: {
        header: {
          PlaceAnAd: 'Разместить объявление',
        },
        home: {
          titleCar: 'Автомобили',
          titleNewCar: 'Новые автомобили',
          titleOldCar: 'Автомобили с пробегом',
          buttonAll: 'Все',
          buttonNew: 'Новые',
          buttonOld: 'С пробегом',
          buttonReset: 'сбросить Х',
          dropdownBrand: 'Марка',
          dropdownAnyBrand: 'Любая',
          dropdownModel: 'Модель',
          dropdowAnyModel: 'Любая',
          inputYearFrom: 'Год от',
          inputYearTo: 'до',
          inputPriceFrom: 'Цена от',
          inputPriceTo: 'до',
          inputKmageFrom: 'Пробег от',
          inputKmageTo: 'до',
          inputHpFrom: 'Мощность (л.с.) от',
          inputHpTo: 'до',
          advertFinds: 'Объявлений найдено:',
          advertOnPage: 'Объявлений на странице:',
          previous: 'предыдущая',
          next: 'следующая',
        },
        table: {
          new: 'НОВЫЙ',
          hp: 'Мощность',
          hpMetric: 'л.с.',
          year: 'Год выпуска:',
          kmage: 'Пробег:',
        },
      },
    },
    en: {
      translation: {
        header: {
          PlaceAnAd: 'Place an ad',
        },
        home: {
          titleCar: 'Cars',
          titleNewCar: 'New cars',
          titleOldCar: 'Used cars',
          buttonAll: 'All',
          buttonNew: 'New',
          buttonOld: 'Used',
          buttonReset: 'reset all X',
          dropdownBrand: 'Brand',
          dropdownAnyBrand: 'Any brand',
          dropdownModel: 'Model',
          dropdowAnyModel: 'Any model',
          inputYearFrom: 'Year from',
          inputYearTo: 'to',
          inputPriceFrom: 'Price from',
          inputPriceTo: 'to',
          inputKmageFrom: 'Kmage from',
          inputKmageTo: 'to',
          inputHpFrom: 'Horsepower from',
          inputHpTo: 'to',
          advertFinds: 'Listings found:',
          advertOnPage: 'Ads per page:',
          previous: 'previous',
          next: 'next',
        },
        table: {
          new: 'NEW',
          hp: 'Horsepower:',
          hpMetric: '',
          year: 'Year:',
          kmage: 'Kmage:',
        },
      },
    },
  },
})

export default i18n
