'use client'

// import { useNavigate } from 'react-router-dom'
import { useContext } from 'react'
import styled from 'styled-components'
// import Context from '../../context'
import i18next from '../../i18n'

const Container = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: 60px;
  min-height: 60px;
  background-color: #e95b5b;
  justify-content: center;
`
const Content = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  max-width: 980px;
  align-items: center;
  margin: 0 12px 0 12px;
  justify-content: space-between;
`
const Logo = styled.div`
  margin: 0 12px 0 0;
  color: white;
  font-size: 20px;
  font-weight: 600;
  cursor: pointer;
`
const Buttons = styled.div`
  display: flex;
  flex-direction: row;
  margin: 0 0 0 12px;
`
const ChangeLang = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-weight: 600;
  margin-right: 6px;
  height: 30px;
  width: 30px;
  background-color: white;
  cursor: pointer;
`
const PostCar = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100px;
  height: 30px;
  color: white;
  font-size: 12px;
  font-weight: 600;
  text-align: center;
  cursor: pointer;
`
// <Logo onClick={() => navigate('/')}>LOGO</Logo>
// <PostCar onClick={() => navigate('/addcar')}>

function Header() {
  //const navigate = useNavigate()
  //const { setCurrentLang } = useContext(Context)
  return (
    <Container>
      <Content>
        <Logo>LOGO</Logo>
        <Buttons>
          <ChangeLang
            onClick={() => {
              i18next.language === 'ru'
                ? i18next.changeLanguage('en')
                : i18next.changeLanguage('ru')
              // setCurrentLang(i18next.language)
            }}
          >
            {i18next.language.toUpperCase()}
          </ChangeLang>
          <PostCar>{i18next.t('header.PlaceAnAd')} </PostCar>
        </Buttons>
      </Content>
    </Container>
  )
}
export default Header
