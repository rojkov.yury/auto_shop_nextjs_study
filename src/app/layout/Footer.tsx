'use client'

import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  width: 100%;
  height: 40px;
  background-color: #292929;
  color: white;
  align-items: center;
  justify-content: center;
  position: fixed;
  bottom: 0;
`

function Footer() {
  return <Container>© 1990-2023</Container>
}

export default Footer
