import { useState } from 'react'
import styled from 'styled-components'
import i18next from '../../i18n'

const Container = styled.div`
  display: flex;
  flex-direction: row;
  margin: 6px 6px 6px 0;
`
const KmageContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  .right {
    border-left: none;
  }
  .active {
    color: white;
    background-color: #e95b5b;
    border-color: #e95b5b;
    pointer-events: none;
  }
`
const KmageButton = styled.button`
  width: 110px;
  height: 40px;
  background-color: white;
  border: 1px solid #d4d4d4;
  color: black;
  &:hover {
    background-color: #b34c4c;
    border-color: #b34c4c;
    color: white;
    cursor: pointer;
  }
`
const ResetContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  justify-content: flex-end;
`
const ResetButton = styled.div`
  user-select: none;
  color: #e95b5b;
  &:hover {
    color: red;
    cursor: pointer;
  }
`
interface ButtonsProps {
  setAge: (age: 'all' | 'new' | 'old') => void
  setBrandValue: (brandValue: string) => void
  setModelValue: (brandValue: string) => void
  setYearFromValue: (yearFromValue?: string) => void
  setYearToValue: (yearToValue?: string) => void
  setPriceFromValue: (priceFromValue?: string) => void
  setPriceToValue: (priceToValue?: string) => void
  setKmageFromValue: (kmageFromValue?: string) => void
  setKmageToValue: (kmageToValue?: string) => void
  setHpFromValue: (hpFromValue?: string) => void
  setHpToValue: (hpToValue?: string) => void
  setCurrentPage: (currentPage: number) => void
}

function Buttons(props: ButtonsProps) {
  const [newCarsSelect, setNewCarsSelect] = useState<boolean>(true)
  const [oldCarsSelect, setOldCarsSelect] = useState<boolean>(true)

  return (
    <Container>
      <KmageContainer>
        <KmageButton
          onClick={() => {
            setOldCarsSelect(true)
            setNewCarsSelect(true)
            props.setAge('all')
          }}
          className={`${newCarsSelect && oldCarsSelect ? 'active' : ''}`}
        >
          {i18next.t('home.buttonAll')}
        </KmageButton>
        <KmageButton
          onClick={() => {
            setOldCarsSelect(false)
            setNewCarsSelect(true)
            props.setAge('new')
            props.setCurrentPage(1)
          }}
          className={`right ${newCarsSelect && !oldCarsSelect ? 'active' : ''}`}
        >
          {i18next.t('home.buttonNew')}
        </KmageButton>
        <KmageButton
          onClick={() => {
            setNewCarsSelect(false)
            setOldCarsSelect(true)
            props.setAge('old')
            props.setCurrentPage(1)
          }}
          className={`right ${!newCarsSelect && oldCarsSelect ? 'active' : ''}`}
        >
          {i18next.t('home.buttonOld')}
        </KmageButton>
      </KmageContainer>
      <ResetContainer>
        <ResetButton
          onClick={() => {
            props.setAge('all')
            setOldCarsSelect(true)
            setNewCarsSelect(true)
            props.setBrandValue('')
            props.setModelValue('')
            props.setYearFromValue('')
            props.setYearToValue('')
            props.setPriceFromValue('')
            props.setPriceToValue('')
            props.setKmageFromValue('')
            props.setKmageToValue('')
            props.setHpFromValue('')
            props.setHpToValue('')
            props.setCurrentPage(1)
          }}
        >
          {i18next.t('home.buttonReset')}
        </ResetButton>
      </ResetContainer>
    </Container>
  )
}

export default Buttons
