// import { useNavigate } from 'react-router-dom'
import styled from 'styled-components'
import { Car } from '../../contracts/cars' //interface
import i18next from '../../i18n'

const Container = styled.div`
  display: grid;
  grid-template-areas: 'a b d' 'a c d';
  //grid-template-areas: 'a' 'b' 'c' 'd';
  grid-template-columns: 185px auto 200px;
  background-color: #ececec;
  border-radius: 8px;
  margin: 6px;
  cursor: pointer;
`
const CarImage = styled.img`
  width: 185px;
  height: 150px;
  border-radius: 6px;
  grid-area: a;
`
const CarName = styled.div`
  //background-color: lightcyan;
  height: 50px;
  grid-area: b;
`
const CarNameText = styled.div`
  display: flex;
  flex-direction: row;
  font-size: large;
  font-weight: 600;
  padding: 6px;
`
const CarInfo = styled.div`
  //background-color: lightgoldenrodyellow;
  height: 100px;
  grid-area: c;
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  justify-content: flex-end;
`
const CarInfoText = styled.div`
  padding: 0 0 6px 6px;
`
const CarPrice = styled.div`
  height: 150px;
  grid-area: d;
  text-align: center;
`
const CarPriceText = styled.div`
  font-size: larger;
  font-weight: 600;
  padding: 6px;
  color: white;
  background-color: rgb(54, 181, 85);
  border-radius: 8px;
  margin: 6px 6px 6px 0;
`
const CarNewText = styled.div`
  display: flex;
  align-items: center;
  padding: 0 6px 0 6px;
  margin: 0 6px 0 6px;
  color: #555555;
  background-color: #f8e0e0;
  border-radius: 8px;
  font-size: small;
`
function Table(props: { carsList?: Car[] }) {
  // const navigate = useNavigate()

  // <Container key={el.id} onClick={() => navigate('/' + el.id)}>

  return (
    <>
      {props?.carsList?.map((el) => {
        return (
          <Container key={el.id}>
            <CarImage src={el.img[0]} alt="нет изображения" />
            <CarName>
              <CarNameText>
                {el.brand} {el.model}{' '}
                {el.new && <CarNewText>{i18next.t('table.new')}</CarNewText>}
              </CarNameText>
            </CarName>
            <CarInfo>
              <CarInfoText>
                {i18next.t('table.hp')} {el.hp} {i18next.t('table.hpMetric')}
              </CarInfoText>
              <CarInfoText>
                {i18next.t('table.year')} {el.year}
              </CarInfoText>
              {!el.new && (
                <CarInfoText>
                  {i18next.t('table.kmage')}{' '}
                  {el.kmage
                    .toString()
                    .replace(
                      /(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g,
                      '$1' + ' '
                    )}{' '}
                  км
                </CarInfoText>
              )}
            </CarInfo>
            <CarPrice>
              <CarPriceText>
                {el.price
                  .toString()
                  .replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, '$1' + ' ')}{' '}
                ₽
              </CarPriceText>
            </CarPrice>
          </Container>
        )
      })}
    </>
  )
}
export default Table
