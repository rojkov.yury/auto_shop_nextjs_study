import { useState } from 'react'
import styled from 'styled-components'
import i18next from '../../i18n'

const Container = styled.div`
  display: flex;
  flex-direction: row;
  height: 40px;
  align-items: center;
  margin: 6px;
  justify-content: space-between;
`
const TextContainer = styled.div`
  display: flex;
  flex-direction: row;
  white-space: nowrap;
`
const PagingContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  white-space: nowrap;
  justify-content: right;
  align-items: center;
`
const RowsNumberContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  border: 1px solid #d4d4d4;
  outline: none; /* отключает выделение рамки при нажатии клафиш в фокусе, не убирать! */
  cursor: pointer;
`
const RowsNumber = styled.div`
  position: relative;
  display: flex;
  height: 20px;
  width: 20px;
  padding: 4px;
  justify-content: center;
  align-items: center;
`
// const arrowIcon = require('../../../icons/DDarrow.svg').default as string

const CustomImg = styled.div`
  background-repeat: no-repeat;
  background-position: center;
  height: 20px;
  width: 20px;
  padding: 4px;
`
const Menu = styled.div`
  display: block;
  position: absolute;
  margin-top: 100px;
  list-style-type: none;
  border-right: 1px solid #d4d4d4;
  border-bottom: 1px solid #d4d4d4;
  border-left: 1px solid #d4d4d4;
  background-color: white;
  width: 100%;
  left: -1px;
  overflow: hidden;
  font-size: 12px;
  z-index: 10;
  white-space: nowrap;
`
const Line = styled.div`
  padding: 3px 3px 3px 6px;
  font-size: 14px;
  &:hover {
    background-color: #ececec;
    cursor: pointer;
  }
`
interface InfoProps {
  setRowsPerPage: (rowsPerPage: number) => void
  rowsPerPage: number
  carsQuantityAfterFilters: number
  setCurrentPage: (currentPage: number) => void
}

function Info(props: InfoProps) {
  const [open, setOpen] = useState<boolean>(false)
  return (
    <Container>
      <TextContainer>
        {i18next.t('home.advertFinds')} {props.carsQuantityAfterFilters}
      </TextContainer>
      <PagingContainer>
        <div>{i18next.t('home.advertOnPage')}&nbsp;</div>
        <RowsNumberContainer
          onClick={() => setOpen(!open)}
          tabIndex={0}
          onBlur={() =>
            open
              ? setTimeout(() => {
                  setOpen(!open)
                }, 150)
              : ''
          }
        >
          <RowsNumber>{props.rowsPerPage}</RowsNumber>
          <CustomImg />
          {open && (
            <Menu>
              {props.rowsPerPage !== 3 && (
                <Line
                  onClick={() => {
                    props.setRowsPerPage(3)
                    props.setCurrentPage(1)
                  }}
                >
                  3
                </Line>
              )}
              {props.rowsPerPage !== 5 && (
                <Line
                  onClick={() => {
                    props.setRowsPerPage(5)
                    props.setCurrentPage(1)
                  }}
                >
                  5
                </Line>
              )}
              {props.rowsPerPage !== 10 && (
                <Line
                  onClick={() => {
                    props.setRowsPerPage(10)
                    props.setCurrentPage(1)
                  }}
                >
                  10
                </Line>
              )}
              {props.rowsPerPage !== 30 && (
                <Line
                  onClick={() => {
                    props.setRowsPerPage(30)
                    props.setCurrentPage(1)
                  }}
                >
                  30
                </Line>
              )}
            </Menu>
          )}
        </RowsNumberContainer>
      </PagingContainer>
    </Container>
  )
}
export default Info
