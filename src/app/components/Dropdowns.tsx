import { useState } from 'react'
import styled from 'styled-components'
import { useFullCarsList } from '../../contracts/cars'
import i18next from '../../i18n'

const Container = styled.div`
  display: flex;
  flex-direction: row;
`
//const arrowIcon = require('../../icons/DDarrow.svg').default as string

const DropdownContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  flex-grow: 1;
  margin: 6px 6px 6px 0;
  border: 1px solid #d4d4d4;
  outline: none; /* отключает выделение рамки при нажатии клафиш в фокусе, не убирать! */
  .active {
    background-color: #e077773b;
  }
  .deactive {
    background-color: #8f8f8f3a;
    pointer-events: none;
    opacity: 0.4;
  }
`
const DisableInput = styled.input`
  border: none;
  width: 100%;
  padding: 12px 0 12px 12px;
  overflow: hidden;
  text-align: left;
  font-size: 16px;
  font-weight: 500;
  white-space: nowrap; /* Запрещаем перенос строк */
  text-overflow: ellipsis; /* Добавляем многоточие */
  color: #757575;
  outline: none;
  cursor: pointer;
`
const Menu = styled.div`
  display: block;
  position: absolute;
  margin-top: 40px;
  list-style-type: none;
  border-right: 1px solid #d4d4d4;
  border-bottom: 1px solid #d4d4d4;
  border-left: 1px solid #d4d4d4;
  background-color: white;
  width: 100%;
  left: -1px;
  overflow: hidden;
  font-size: 12px;
  z-index: 10;
  white-space: nowrap;
`
const Line = styled.div`
  padding: 3px 3px 3px 13px;
  font-size: 14px;
  &:hover {
    background-color: #ececec;
    cursor: pointer;
  }
`
const CustomImg = styled.div`
  background-repeat: no-repeat;
  background-position: center;
  height: 35px;
  width: 35px;
  padding: 4px;
`
interface DropdownsProps {
  brandValue?: string // значение бренда (марка)
  setBrandValue: (brandValue: string) => void // изменить значение бренда (марка)
  modelValue?: string // значение модели
  setModelValue: (brandValue: string) => void // изменить значение модели
  setCurrentPage: (currentPage: number) => void // изменить текущую пагин страницу
}

interface DropdownsImports {
  uniqueBrand?: string[] // список уникальных брендов
  uniqueModelForSelectBrand?: string[] // список уникальных моделей для выбранного бренда
}

function Dropdowns({
  brandValue,
  setBrandValue,
  modelValue,
  setModelValue,
  setCurrentPage,
}: DropdownsProps) {
  const [openBrand, setOpenBrand] = useState<boolean>(false)
  const [openModel, setOpenModel] = useState<boolean>(false)
  //массив уникальных значений бренда и полный список:
  const { uniqueBrand, uniqueModelForSelectBrand }: DropdownsImports =
    useFullCarsList({
      brandValue,
    })

  return (
    <Container>
      <DropdownContainer
        onClick={() => setOpenBrand(!openBrand)}
        tabIndex={0}
        onBlur={() =>
          openBrand
            ? setTimeout(() => {
                setOpenBrand(!openBrand)
              }, 200)
            : ''
        }
      >
        <DisableInput
          placeholder={i18next.t('home.dropdownBrand').toString()}
          value={brandValue}
          className={brandValue ? 'active' : ''}
          readOnly
        />
        <CustomImg className={brandValue ? 'active' : ''} />
        {openBrand && (
          <Menu>
            {brandValue && (
              <Line
                onClick={() => {
                  setBrandValue('')
                  setModelValue('')
                  setCurrentPage(1)
                }}
              >
                {i18next.t('home.dropdownAnyBrand')}
              </Line>
            )}
            {uniqueBrand.map((el) => (
              <Line
                key={el}
                onClick={() => {
                  setBrandValue(el)
                  setModelValue('')
                  setCurrentPage(1)
                }}
              >
                {el}
              </Line>
            ))}
          </Menu>
        )}
      </DropdownContainer>
      <DropdownContainer
        onClick={() => setOpenModel(!openModel)}
        tabIndex={0}
        onBlur={() =>
          openModel
            ? setTimeout(() => {
                setOpenModel(!openModel)
              }, 100)
            : ''
        }
      >
        <DisableInput
          placeholder={i18next.t('home.dropdownModel').toString()}
          value={modelValue}
          className={!brandValue ? 'deactive' : modelValue ? 'active' : ''}
          readOnly
        />
        <CustomImg
          className={!brandValue ? 'deactive' : modelValue ? 'active' : ''}
        />
        {openModel && brandValue && (
          <Menu>
            {modelValue && (
              <Line
                onClick={() => {
                  setModelValue('')
                  setCurrentPage(1)
                }}
              >
                {i18next.t('home.dropdowAnyModel')}
              </Line>
            )}
            {uniqueModelForSelectBrand.map((el) => (
              <Line
                key={el}
                onClick={() => {
                  setModelValue(el)
                  setCurrentPage(1)
                }}
              >
                {el}
              </Line>
            ))}
          </Menu>
        )}
      </DropdownContainer>
    </Container>
  )
}

export default Dropdowns
