import styled from 'styled-components'

//const notFoundIcon = require('../../../icons/NotFound.svg').default as string
const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  margin-top: 60px;
`
const BigText = styled.div`
  font-size: 30px;
`
const SmallText = styled.div`
  font-size: 16px;
`
const CustomImg = styled.div`
  background-repeat: no-repeat;
  background-position: center;
  height: 250px;
  width: 250px;
`

function NotFound() {
  return (
    <Container>
      <BigText>Объявлений не найдено</BigText>
      <SmallText>Попробуйте изменить критерии поиска</SmallText>
      <CustomImg />
    </Container>
  )
}
export default NotFound
