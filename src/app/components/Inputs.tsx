import styled from 'styled-components'
import i18next from '../../i18n'

const Container = styled.div`
  width: 100%;
`
const InputContainer = styled.div`
  display: flex;
  flex-direction: row;
`
const InputLineContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  margin: 6px 6px 6px 0;
  .active {
    background-color: #f8e0e0;
  }
  .left {
    border-right: none;
  }
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
`
const CustomInput = styled.input`
  width: 100%;
  outline: none;
  height: 40px;
  padding: 0 0 0 12px;
  font-size: 16px;
  border-right: 1px solid #d4d4d4;
  border-left: 1px solid #d4d4d4;
  border-bottom: 1px solid #d4d4d4;
  border-top: 1px solid #d4d4d4;
`
interface InputsProps {
  setYearFromValue: (yearFromValue?: string) => void
  setYearToValue: (yearToValue?: string) => void
  setPriceFromValue: (priceFromValue?: string) => void
  setPriceToValue: (priceToValue?: string) => void
  setKmageFromValue: (kmageFromValue?: string) => void
  setKmageToValue: (kmageToValue?: string) => void
  setHpFromValue: (hpFromValue?: string) => void
  setHpToValue: (hpToValue?: string) => void
  yearToValue?: string
  yearFromValue?: string
  priceFromValue?: string
  priceToValue?: string
  kmageFromValue?: string
  kmageToValue?: string
  hpFromValue?: string
  hpToValue?: string
  setCurrentPage: (currentPage: number) => void // задаем текущую страницу пагинации
}

function Inputs(props: InputsProps) {
  return (
    <Container>
      <InputContainer>
        <InputLineContainer>
          <CustomInput
            value={props.yearFromValue}
            type="number"
            className={props.yearFromValue ? 'left active' : 'left'}
            placeholder={i18next.t('home.inputYearFrom').toString()}
            onChange={(e) => {
              props.setYearFromValue(e.target.value)
              props.setCurrentPage(1)
            }}
          />
          <CustomInput
            value={props.yearToValue}
            type="number"
            className={props.yearToValue ? 'active' : ''}
            placeholder={i18next.t('home.inputYearTo').toString()}
            onChange={(e) => {
              props.setYearToValue(e.target.value)
              props.setCurrentPage(1)
            }}
          />
        </InputLineContainer>
        <InputLineContainer>
          <CustomInput
            value={props.priceFromValue}
            className={props.priceFromValue ? 'left active' : 'left'}
            type="number"
            placeholder={i18next.t('home.inputPriceFrom').toString()}
            onChange={(e) => {
              props.setPriceFromValue(e.target.value)
              props.setCurrentPage(1)
            }}
          />
          <CustomInput
            value={props.priceToValue}
            className={props.priceToValue ? 'active' : ''}
            placeholder={i18next.t('home.inputPriceTo').toString()}
            type="number"
            onChange={(e) => {
              props.setPriceToValue(e.target.value)
              props.setCurrentPage(1)
            }}
          />
        </InputLineContainer>
      </InputContainer>
      <InputContainer>
        <InputLineContainer>
          <CustomInput
            value={props.kmageFromValue}
            className={props.kmageFromValue ? 'left active' : 'left'}
            type="number"
            placeholder={i18next.t('home.inputKmageFrom').toString()}
            onChange={(e) => {
              props.setKmageFromValue(e.target.value)
              props.setCurrentPage(1)
            }}
          />
          <CustomInput
            value={props.kmageToValue}
            className={props.kmageToValue ? 'active' : ''}
            placeholder={i18next.t('home.inputKmageTo').toString()}
            type="number"
            onChange={(e) => {
              props.setKmageToValue(e.target.value)
              props.setCurrentPage(1)
            }}
          />
        </InputLineContainer>
        <InputLineContainer>
          <CustomInput
            value={props.hpFromValue}
            className={props.hpFromValue ? 'left active' : 'left'}
            type="number"
            placeholder={i18next.t('home.inputHpFrom').toString()}
            onChange={(e) => {
              props.setHpFromValue(e.target.value)
              props.setCurrentPage(1)
            }}
          />
          <CustomInput
            value={props.hpToValue}
            className={props.hpToValue ? 'active' : ''}
            placeholder={i18next.t('home.inputHpTo').toString()}
            type="number"
            onChange={(e) => {
              props.setHpToValue(e.target.value)
              props.setCurrentPage(1)
            }}
          />
        </InputLineContainer>
      </InputContainer>
    </Container>
  )
}

export default Inputs
