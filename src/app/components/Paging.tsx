import styled from 'styled-components'
import i18next from '../../i18n'

const Container = styled.div`
  display: flex;
  flex-direction: row;
  margin-left: auto;
  margin-right: auto;
  align-items: center;
  .deactive {
    pointer-events: none;
    opacity: 0.4;
  }
`
const Direction = styled.div`
  margin: 6px;
  padding: 6px;
  cursor: pointer;
  &&:hover {
    background-color: #f8e0e0;
    border-radius: 15px;
  }
`
const CurrentPageInfo = styled.div`
  margin: 6px;
  padding: 6px;
`

interface PagingProps {
  currentPage: number // текущая страница пагинации
  setCurrentPage: (currentPage: number) => void // задаем текущую страницу пагинации
  carsQuantityAfterFilters: number // количество объявлении после применении фильтра
  rowsPerPage: number //объектов на страницы пагинации
}

function Paging(props: PagingProps) {
  const pagePagingQuantity = Math.ceil(
    props.carsQuantityAfterFilters / props.rowsPerPage
  )
  return (
    <Container>
      <Direction
        className={props.currentPage <= 1 ? 'deactive' : ''}
        onClick={() => props.setCurrentPage(props.currentPage - 1)}
      >
        {i18next.t('home.previous')}
      </Direction>
      <CurrentPageInfo>
        {props.currentPage} из {pagePagingQuantity}
      </CurrentPageInfo>
      <Direction
        className={props.currentPage >= pagePagingQuantity ? 'deactive' : ''}
        onClick={() => props.setCurrentPage(props.currentPage + 1)}
      >
        {i18next.t('home.next')}
      </Direction>
    </Container>
  )
}
export default Paging
