'use client'

import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`
const CustomInputContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin: 6px 0 6px 0;
  .filled {
    background-color: #fadfdf;
  }
`
const CustomInput = styled.input`
  width: 100%;
  outline: none;
  height: 40px;
  padding: 0 0 0 12px;
  font-size: 16px;
  border: 1px solid #d4d4d4;
`
interface AddImagesProps {
  addImg1: string
  setAddImg1: (addImg1: string) => void
  addImg2: string
  setAddImg2: (addImg2: string) => void
  addImg3: string
  setAddImg3: (addImg3: string) => void
  addImg4: string
  setAddImg4: (addImg4: string) => void
}

function AddImages(props: AddImagesProps) {
  return (
    <Container>
      <CustomInputContainer>
        <CustomInput
          className={props.addImg1 ? 'filled' : ''}
          placeholder="Ссылка на фотографию №1"
          value={props.addImg1}
          onChange={(e) => props.setAddImg1(e.target.value)}
        />
      </CustomInputContainer>
      <CustomInputContainer>
        <CustomInput
          className={props.addImg2 ? 'filled' : ''}
          placeholder="Ссылка на фотографию №2"
          value={props.addImg2}
          onChange={(e) => props.setAddImg2(e.target.value)}
        />
      </CustomInputContainer>
      <CustomInputContainer>
        <CustomInput
          className={props.addImg3 ? 'filled' : ''}
          placeholder="Ссылка на фотографию №3"
          value={props.addImg3}
          onChange={(e) => props.setAddImg3(e.target.value)}
        />
      </CustomInputContainer>
      <CustomInputContainer>
        <CustomInput
          className={props.addImg4 ? 'filled' : ''}
          placeholder="Ссылка на фотографию №4"
          value={props.addImg4}
          onChange={(e) => props.setAddImg4(e.target.value)}
        />
      </CustomInputContainer>
    </Container>
  )
}

export default AddImages
