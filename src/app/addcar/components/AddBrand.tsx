'use client'

import styled from 'styled-components'

const CustomInputContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  margin: 6px 6px 6px 6px;
  .readOnly {
    cursor: pointer;
  }
  .filled {
    background-color: #fadfdf;
  }
`
const CustomInput = styled.input`
  width: 100%;
  outline: none;
  height: 40px;
  padding: 0 0 0 12px;
  font-size: 16px;
  border: 1px solid #d4d4d4;
`
const BrandSelectContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
`
const BrandSelect = styled.div`
  display: flex;
  margin: 6px;
  width: 120px;
  height: 60px;
  font-size: 14px;
  white-space: nowrap;
  align-items: center;
  justify-content: center;
  background-color: #f8e0e0;
  border-radius: 15px;
  cursor: pointer;
`
interface AddBrandProps {
  addBrand?: string // значение бренда (марка)
  setAddBrand: (brandValue: string) => void // значение бренда (марка)
  addBrandMenu?: boolean // открыто ли меню с выбором бренда?
  setAddBrandMenu: (brandValue: boolean) => void // открыто ли меню с выбором бренда?
  uniqueBrand: string[] // массив уникальных брендов из общей БД
  setAddModel: (addModel: string) => void // значение модели
}

function AddBrand(props: AddBrandProps) {
  return (
    <>
      <CustomInputContainer>
        <CustomInput
          readOnly
          className={props.addBrand ? 'filled readOnly' : 'readOnly'}
          placeholder="Марка"
          value={props.addBrand}
          onClick={() => props.setAddBrandMenu(true)}
          onChange={(e) => {
            props.setAddBrand(e.target.value)
          }}
        />
      </CustomInputContainer>
      <BrandSelectContainer>
        {props.addBrandMenu &&
          props.uniqueBrand.map(
            (el) =>
              el !== props.addBrand && (
                <BrandSelect
                  key={el}
                  onClick={() => {
                    props.setAddBrand(el)
                    props.setAddBrandMenu(false)
                    props.setAddModel('')
                  }}
                >
                  {el}
                </BrandSelect>
              )
          )}
      </BrandSelectContainer>
    </>
  )
}
export default AddBrand
