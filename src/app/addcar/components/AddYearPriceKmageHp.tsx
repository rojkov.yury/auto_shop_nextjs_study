'use client'

import { useState } from 'react'
import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`
const Title = styled.div`
  display: flex;
  margin: 16px 0 0 0;
  font-size: 20px;
  justify-content: left;
  align-items: center;
  height: 40px;
  width: 100%;
  white-space: nowrap;
`
const Direction = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  .leftside {
    margin: 6px 6px 6px 0;
  }
  .rightside {
    margin: 6px 0 6px 6px;
  }
`
const CustomInputContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  width: 100%;
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  .filled {
    background-color: #fadfdf;
  }
  .filledUpper {
    border-bottom: 1px solid #d4d4d4;
  }
  .deactive {
    background-color: #f5f5f5;
    pointer-events: none;
    border-color: #f5f5f5;
    opacity: 0.8;
  }
`
const InputUpper = styled.div`
  position: absolute;
  display: flex;
  background-color: white;
  font-size: small;
  padding: 1px;
  top: -9px;
  left: 10px;
  border: 1px solid white;
`
const CustomInput = styled.input`
  width: 100%;
  outline: none;
  height: 40px;
  padding: 0 0 0 12px;
  font-size: 16px;
  border: 1px solid #d4d4d4;
`
const CheckboxContainer = styled.div`
  display: flex;
  align-items: center;
  .checkboxSelected {
    background-color: #fadfdf;
  }
`
const Checkbox = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid #d4d4d4;
  width: 20px;
  height: 20px;
  margin: 6px;
  cursor: pointer;
`
const CheckboxIcon = require('../../../icons/Checkbox.svg').default as string

const CustomImg = styled.div`
  background-image: url(${CheckboxIcon});
  background-repeat: no-repeat;
  background-position: center;
  background-size: 100%;
  width: 17px;
  height: 17px;
`
const CheckboxText = styled.div`
  display: flex;
  white-space: nowrap;
  font-size: 14px;
`
interface AddYearPriceKmageHpProps {
  addYear?: string
  setAddYear: (addYear: string) => void
  addKmage?: string
  setAddKmage: (addKmage: string) => void
  addPrice?: string
  setAddPrice: (addPrice: string) => void
  addHp?: string
  setAddHp: (addHp: string) => void
  addNew: boolean
  setAddNew: (addNew: boolean) => void
}

function AddYearPriceKmageHp(props: AddYearPriceKmageHpProps) {
  const [withoutKmage, setWithoutKmage] = useState<boolean>(false)
  return (
    <Container>
      <Title>Характеристики</Title>
      <Direction>
        <CustomInputContainer className="leftside">
          <CustomInput
            className={props.addPrice ? 'filled' : ''}
            placeholder="₽ руб."
            value={props.addPrice}
            onChange={(e) => {
              props.setAddPrice(e.target.value)
            }}
          />
          <InputUpper className={props.addPrice ? 'filledUpper' : ''}>
            Стоимость
          </InputUpper>
        </CustomInputContainer>
        <CustomInputContainer className="rightside">
          <CustomInput
            className={
              props.addKmage
                ? 'filled' + (withoutKmage ? 'deactive' : '')
                : withoutKmage
                ? 'deactive'
                : ''
            }
            placeholder="км"
            value={props.addKmage}
            onChange={(e) => {
              props.setAddKmage(e.target.value)
            }}
          />
          <InputUpper
            className={
              props.addKmage
                ? 'filledUpper' + (withoutKmage ? 'deactive' : '')
                : withoutKmage
                ? 'deactive'
                : ''
            }
          >
            Пробег
          </InputUpper>
          <CheckboxContainer>
            <Checkbox
              className={withoutKmage ? 'checkboxSelected' : ''}
              onClick={() => {
                setWithoutKmage(!withoutKmage)
                props.setAddKmage('')
                props.setAddNew(!props.addNew)
              }}
            >
              {withoutKmage && <CustomImg />}
            </Checkbox>
            <CheckboxText>без пробега</CheckboxText>
          </CheckboxContainer>
        </CustomInputContainer>
      </Direction>
      <Direction>
        <CustomInputContainer className="leftside">
          <CustomInput
            className={props.addYear ? 'filled' : ''}
            type="number"
            placeholder=""
            value={props.addYear}
            onChange={(e) => {
              props.setAddYear(e.target.value)
            }}
          />
          <InputUpper className={props.addYear ? 'filledUpper' : ''}>
            Год выпуска
          </InputUpper>
        </CustomInputContainer>
        <CustomInputContainer className="rightside">
          <CustomInput
            className={props.addHp ? 'filled' : ''}
            type="number"
            placeholder="л.с."
            value={props.addHp}
            onChange={(e) => {
              props.setAddHp(e.target.value)
            }}
          />
          <InputUpper className={props.addHp ? 'filledUpper' : ''}>
            Мощность
          </InputUpper>
        </CustomInputContainer>
      </Direction>
    </Container>
  )
}
export default AddYearPriceKmageHp
