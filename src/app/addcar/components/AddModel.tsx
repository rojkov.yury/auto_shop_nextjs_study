'use client'

import styled from 'styled-components'

const CustomInputContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  margin: 6px 6px 6px 6px;
  .filled {
    background-color: #fadfdf;
  }
`
const CustomInput = styled.input`
  width: 100%;
  outline: none;
  height: 40px;
  padding: 0 0 0 12px;
  font-size: 16px;
  border: 1px solid #d4d4d4;
`
const MenuContainer = styled.div`
  display: flex;
  position: relative;
  flex-direction: row;
  width: 100%;
  background-color: white;
  top: -8px;
`
const Menu = styled.div`
  display: flex;
  flex-direction: column;
  border-right: 1px solid #d4d4d4;
  border-bottom: 1px solid #d4d4d4;
  border-left: 1px solid #d4d4d4;
  width: 100%;
  left: -1px;
  overflow: hidden;
  font-size: 12px;
  white-space: nowrap;
  .filled {
    background-color: #fadfdf;
  }
`
const Line = styled.div`
  padding: 3px 3px 3px 13px;
  font-size: 14px;
  &:hover {
    background-color: #e4bebe;
    cursor: pointer;
  }
`
interface AddModelProps {
  addModel: string // значение модели
  setAddModel: (addModel: string) => void // значение модели
  setAddBrandMenu: (brandValue: boolean) => void // открыто ли меню с выбором бренда?
  uniqueModelForSelectBrand: string[] // уникальные значения модели из БД для выбранной марки
  openModelMenu: boolean // открыто ли меню с выбором марки?
  setOpenModelMenu: (openModel: boolean) => void // открыто ли меню с выбором марки?
}

function AddModel(props: AddModelProps) {
  const filteredModels = props.uniqueModelForSelectBrand.filter(
    (el) => el.toUpperCase().indexOf(props.addModel.toUpperCase()) > -1
  )
  return (
    <>
      <CustomInputContainer>
        <CustomInput
          autoFocus={true}
          className={props.addModel ? 'filled' : ''}
          tabIndex={0}
          onBlur={() =>
            props.openModelMenu &&
            setTimeout(() => {
              props.setOpenModelMenu(!props.openModelMenu)
            }, 100)
          }
          placeholder="Модель"
          value={props.addModel}
          onClick={() => {
            props.setAddBrandMenu(false)
          }}
          onChange={(e) => {
            props.setAddModel(e.target.value)
          }}
          onFocus={() => props.setOpenModelMenu(true)}
        />
      </CustomInputContainer>
      {!!filteredModels.length && (
        <MenuContainer
          onClick={() => props.setOpenModelMenu(!props.openModelMenu)}
          tabIndex={0}
          onBlur={() =>
            props.openModelMenu &&
            setTimeout(() => {
              props.setOpenModelMenu(!props.openModelMenu)
            }, 100)
          }
        >
          {props.openModelMenu && (
            <Menu>
              {props.uniqueModelForSelectBrand
                .filter(
                  (el) =>
                    el.toUpperCase().indexOf(props.addModel.toUpperCase()) > -1
                )
                .map((el) => (
                  <Line
                    className={props.addModel ? 'filled' : ''}
                    key={el}
                    onClick={() => {
                      props.setAddModel(el)
                    }}
                  >
                    {el}
                  </Line>
                ))}
            </Menu>
          )}
        </MenuContainer>
      )}
    </>
  )
}

export default AddModel
