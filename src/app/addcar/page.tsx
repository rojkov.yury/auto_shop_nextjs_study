'use client'
//2 ссылки онклик
import { useState } from 'react'
//import { useNavigate } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import styled from 'styled-components'
import axios from 'axios'
import { useFullCarsList } from '../../contracts/cars'

import AddBrand from './components/AddBrand'
import AddModel from './components/AddModel'
import AddYearPriceKmageHp from './components/AddYearPriceKmageHp'
import AddImages from './components/AddImages'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 65%;
  max-width: 980px;
  height: 100%;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 40px;
`
const MessageContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  white-space: nowrap;
  margin-top: 30%;
`
const MessageText = styled.div`
  font-size: 24px;
  background-color: #f8e0e0;
  padding: 2px 10px 2px 10px;
  border-radius: 10px;
`
const MessageLink = styled.div`
  font-size: 16px;
  margin: 2px;
  cursor: pointer;
  &:hover {
    color: red;
  }
`
const Title = styled.div`
  display: flex;
  flex-direction: row;
  margin: 40px 0 0 0;
  font-size: 26px;
  justify-content: center;
  align-items: center;
  height: 40px;
  width: 100%;
  white-space: nowrap;
`
const Reset = styled.div`
  display: flex;
  flex-direction: row;
  margin: 0px 0 16px 0;
  font-size: 16px;
  justify-content: center;
  align-items: center;
  white-space: nowrap;
  cursor: pointer;
  &:hover {
    color: red;
  }
`
const PostButton = styled.div`
  display: flex;
  justify-content: center;
  margin: 10px;
  padding: 10px;
  font-size: 30px;
  width: 200px;
  color: white;
  background-color: #e95b5b;
  border-radius: 25px;
  cursor: pointer;
  &:hover {
    background-color: #b34c4c;
  }
`
interface AddCarImports {
  uniqueBrand?: string[] // список уникальных брендов
  uniqueModelForSelectBrand?: string[] // список уникальных моделей для выбранного бренда
}

export default function AddCar() {
  const [addBrand, setAddBrand] = useState<string>('')
  const [addModel, setAddModel] = useState<string>('')
  const [addYear, setAddYear] = useState<string>('')
  const [addNew, setAddNew] = useState<boolean>(false)
  const [addKmage, setAddKmage] = useState<string>('')
  const [addPrice, setAddPrice] = useState<string>('')
  const [addHp, setAddHp] = useState<string>('')
  const [addImg1, setAddImg1] = useState<string>('')
  const [addImg2, setAddImg2] = useState<string>('')
  const [addImg3, setAddImg3] = useState<string>('')
  const [addImg4, setAddImg4] = useState<string>('')
  const [successPost, setSuccessPost] = useState<boolean>(false)
  const [failPost, setFailPost] = useState<boolean>(false)

  const brandValue = addBrand // Атеншен!
  const [addBrandMenu, setAddBrandMenu] = useState<boolean>(true)
  const [openModelMenu, setOpenModelMenu] = useState<boolean>(true)
  const { uniqueBrand, uniqueModelForSelectBrand }: AddCarImports =
    useFullCarsList({ brandValue })
  //const navigate = useNavigate()
  const addReset = () => {
    setAddBrand('')
    setAddModel('')
    setAddYear('')
    setAddNew(false)
    setAddKmage('')
    setAddPrice('')
    setAddHp('')
    setAddImg1('')
    setAddImg2('')
    setAddImg3('')
    setAddImg4('')
    setAddBrandMenu(true)
  }

  return (
    <Container>
      <Helmet>
        <title>Разместить объявление</title>
      </Helmet>
      {!failPost && successPost && (
        <MessageContainer>
          <MessageText>Объявление успешно опубликованно!</MessageText>
          <MessageLink
            onClick={() => {
              setSuccessPost(false)
            }}
          >
            разместить еще одно
          </MessageLink>
          <MessageLink>на главную</MessageLink>
        </MessageContainer>
      )}
      <Title>
        {!successPost && !addBrand
          ? 'Выберите марку вашего автомобиля'
          : addBrand.toUpperCase() + ' ' + addModel}
      </Title>
      {!successPost && addBrand && (
        <Reset onClick={() => addReset()}>(сбросить)</Reset>
      )}
      {!successPost && (
        <AddBrand
          addBrand={addBrand}
          setAddBrand={setAddBrand}
          addBrandMenu={addBrandMenu}
          setAddBrandMenu={setAddBrandMenu}
          uniqueBrand={uniqueBrand}
          setAddModel={setAddModel}
        />
      )}
      {!successPost && addBrand && (
        <AddModel
          addModel={addModel}
          setAddModel={setAddModel}
          setAddBrandMenu={setAddBrandMenu}
          uniqueModelForSelectBrand={uniqueModelForSelectBrand}
          openModelMenu={openModelMenu}
          setOpenModelMenu={setOpenModelMenu}
        />
      )}
      {!successPost && addBrand && addModel && (
        <AddYearPriceKmageHp
          addYear={addYear}
          setAddYear={setAddYear}
          addKmage={addKmage}
          setAddKmage={setAddKmage}
          addPrice={addPrice}
          setAddPrice={setAddPrice}
          addHp={addHp}
          setAddHp={setAddHp}
          addNew={addNew}
          setAddNew={setAddNew}
        />
      )}
      {!successPost &&
        addBrand &&
        addModel &&
        addYear &&
        (addKmage || addNew) &&
        addPrice &&
        addHp && (
          <AddImages
            addImg1={addImg1}
            setAddImg1={setAddImg1}
            addImg2={addImg2}
            setAddImg2={setAddImg2}
            addImg3={addImg3}
            setAddImg3={setAddImg3}
            addImg4={addImg4}
            setAddImg4={setAddImg4}
          />
        )}
      {!successPost &&
        addBrand &&
        addModel &&
        addYear &&
        (addKmage || addNew) &&
        addPrice &&
        addHp &&
        addImg1 &&
        addImg2 &&
        addImg3 &&
        addImg4 && (
          <PostButton
            onClick={() =>
              axios
                .post('http://localhost:3001/cars', {
                  brand: addBrand,
                  model: addModel,
                  year: Number(addYear),
                  new: addNew,
                  kmage: addNew ? 0 : Number(addKmage),
                  price: Number(addPrice),
                  hp: Number(addHp),
                  img: [addImg1, addImg2, addImg3, addImg4],
                })
                .catch(function (err) {
                  console.error(err)
                  setFailPost(true)
                })
                .finally(() => {
                  addReset()
                  setSuccessPost(true)
                })
            }
          >
            AddCar
          </PostButton>
        )}
      {failPost && (
        <MessageContainer>
          <MessageText>Не удалось опубликовать сообщение </MessageText>
          <MessageLink
            onClick={() => {
              setFailPost(false)
              setSuccessPost(false)
            }}
          >
            попробовать снова
          </MessageLink>
          <MessageLink>на главную</MessageLink>
        </MessageContainer>
      )}
    </Container>
  )
}
