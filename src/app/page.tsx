'use client'

import { useState } from 'react'
import { Helmet } from 'react-helmet'
import styled from 'styled-components'

import i18next from '../i18n'
import { useCarsList } from '../contracts/cars'
import Buttons from './components/Buttons'
import Dropdowns from './components/Dropdowns'
import Inputs from './components/Inputs'
import Info from './components/Info'
import Table from './components/Table'
import Paging from './components/Paging'
import NotFound from './components/NotFound'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  margin: 0 10px 40px 10px;
`
const TitleContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin: 6px 6px 6px 0;
  font-size: 26px;
`
const Filters = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin: 6px 6px 6px 0;
`
const Content = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`
export default function Cars() {
  const [currentPage, setCurrentPage] = useState<number>(1)
  const [rowsPerPage, setRowsPerPage] = useState<number>(5)
  const [age, setAge] = useState<'all' | 'new' | 'old'>('all')
  const [brandValue, setBrandValue] = useState<string | undefined>('')
  const [modelValue, setModelValue] = useState<string | undefined>('')
  const [yearFromValue, setYearFromValue] = useState<string | undefined>('')
  const [yearToValue, setYearToValue] = useState<string | undefined>('')
  const [priceFromValue, setPriceFromValue] = useState<string | undefined>('')
  const [priceToValue, setPriceToValue] = useState<string | undefined>('')
  const [kmageFromValue, setKmageFromValue] = useState<string | undefined>('')
  const [kmageToValue, setKmageToValue] = useState<string | undefined>('')
  const [hpFromValue, setHpFromValue] = useState<string | undefined>('')
  const [hpToValue, setHpToValue] = useState<string | undefined>('')

  const { carsList, carsQuantityAfterFilters } = useCarsList({
    currentPage,
    rowsPerPage,
    age,
    brandValue,
    modelValue,
    yearFromValue,
    yearToValue,
    priceFromValue,
    priceToValue,
    kmageFromValue,
    kmageToValue,
    hpFromValue,
    hpToValue,
  })
  return (
    <Container>
      <Helmet>
        <title>Автомобили</title>
      </Helmet>
      <Filters>
        <TitleContainer>
          {age === 'new'
            ? i18next.t(`home.titleNewCar`)
            : age === 'old'
            ? i18next.t(`home.titleOldCar`)
            : i18next.t(`home.titleCar`)}
        </TitleContainer>
        <Buttons
          setAge={setAge}
          setBrandValue={setBrandValue}
          setModelValue={setModelValue}
          setYearFromValue={setYearFromValue}
          setYearToValue={setYearToValue}
          setPriceFromValue={setPriceFromValue}
          setPriceToValue={setPriceToValue}
          setKmageFromValue={setKmageFromValue}
          setKmageToValue={setKmageToValue}
          setHpFromValue={setHpFromValue}
          setHpToValue={setHpToValue}
          setCurrentPage={setCurrentPage}
        />
        <Dropdowns
          brandValue={brandValue}
          setBrandValue={setBrandValue}
          modelValue={modelValue}
          setModelValue={setModelValue}
          setCurrentPage={setCurrentPage}
        />
        <Inputs
          yearFromValue={yearFromValue}
          setYearFromValue={setYearFromValue}
          yearToValue={yearToValue}
          setYearToValue={setYearToValue}
          priceFromValue={priceFromValue}
          setPriceFromValue={setPriceFromValue}
          priceToValue={priceToValue}
          setPriceToValue={setPriceToValue}
          kmageFromValue={kmageFromValue}
          setKmageFromValue={setKmageFromValue}
          kmageToValue={kmageToValue}
          setKmageToValue={setKmageToValue}
          hpFromValue={hpFromValue}
          setHpFromValue={setHpFromValue}
          hpToValue={hpToValue}
          setHpToValue={setHpToValue}
          setCurrentPage={setCurrentPage}
        />
      </Filters>
      {carsQuantityAfterFilters ? (
        <Content>
          <Info
            rowsPerPage={rowsPerPage}
            setRowsPerPage={setRowsPerPage}
            carsQuantityAfterFilters={carsQuantityAfterFilters}
            setCurrentPage={setCurrentPage}
          />
          <Table carsList={carsList} />
          <Paging
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            carsQuantityAfterFilters={carsQuantityAfterFilters}
            rowsPerPage={rowsPerPage}
          />
        </Content>
      ) : (
        <Content>
          <NotFound />
        </Content>
      )}
    </Container>
  )
}
