import { useEffect, useState } from 'react'
// import { useParams } from 'react-router-dom'
import axios from 'axios'

export interface Car {
  id: number
  brand: string
  model: string
  year: number
  new: boolean
  kmage: number
  price: number
  hp: number
  img: string[]
}
export interface CarsListParams {
  currentPage: number
  rowsPerPage: number
  age: 'all' | 'new' | 'old'
  brandValue?: string
  modelValue?: string
  yearFromValue?: string
  yearToValue?: string
  priceFromValue?: string
  priceToValue?: string
  kmageFromValue?: string
  kmageToValue?: string
  hpFromValue?: string
  hpToValue?: string
}
export interface useFullCarsList {
  brandValue?: string
}

const baseURL = 'http://localhost:3001/cars?'

export const useCarsList = (params?: CarsListParams) => {
  // отсортированный массив для выведения таблицы:
  const [carsList, setCarsList] = useState<Car[] | undefined>(undefined)
  // пагинация
  const pagingQuery =
    '_page=' + params?.currentPage + '&_limit=' + params?.rowsPerPage
  // фильтр - новые, с пробегов или все:
  const ageFilter =
    params?.age === 'all'
      ? ''
      : params?.age === 'new'
      ? '&new=true'
      : '&new=false'
  // фильтр - марка:
  const brandFilter = params?.brandValue ? '&brand=' + params?.brandValue : ''
  // фильтр - модель:
  const modelFilter = params?.modelValue ? '&model=' + params?.modelValue : ''
  // фильтр - год:
  const yearFromFilter = params?.yearFromValue
    ? '&year_gte=' + params?.yearFromValue
    : ''
  //
  const yearToFilter = params?.yearToValue
    ? '&year_lte=' + params?.yearToValue
    : ''
  // фильтр - цена:
  const priceFromValue = params?.priceFromValue
    ? '&price_gte=' + params?.priceFromValue
    : ''
  //
  const priceToValue = params?.priceToValue
    ? '&price_lte=' + params?.priceToValue
    : ''
  // фильтр - пробег:
  const kmageFromValue = params?.kmageFromValue
    ? '&kmage_gte=' + params?.kmageFromValue
    : ''
  //
  const kmageToValue = params?.kmageToValue
    ? '&kmage_lte=' + params?.kmageToValue
    : ''
  // фильтр - мощность:
  const hpFromValue = params?.hpFromValue
    ? '&hp_gte=' + params?.hpFromValue
    : ''
  //
  const hpToValue = params?.hpToValue ? '&hp_lte=' + params?.hpToValue : ''
  // Общая строка фильтров:
  const filtresQuery =
    ageFilter +
    brandFilter +
    modelFilter +
    yearFromFilter +
    yearToFilter +
    priceFromValue +
    priceToValue +
    kmageFromValue +
    kmageToValue +
    hpFromValue +
    hpToValue
  // конец фильтров

  // Количество объектов по заданным фильтрами:
  const [carsQuantityAfterFilters, setCarsQuantityAfterFilters] =
    useState<number>(0)

  useEffect(() => {
    //Запрос на формирования основной таблицы:
    axios
      .get(baseURL + pagingQuery + filtresQuery)
      .then((res) => setCarsList(res.data))
    //Запрос на количество объектов по заданным фильтрами:
    axios
      .get(baseURL + filtresQuery)
      .then((res) => setCarsQuantityAfterFilters(res.data.length))
  }, [JSON.stringify(params)])
  return { carsList, carsQuantityAfterFilters }
}

// для заполнении вариации в фильтрах нужен полный список всех элементов,
// например, все уникальные значения марки или модели.
// В теории, этот список надо формировать на бэке,
// и вообще, задуматься, стоит ли давать возможность вносить новые значения в эти поля
// и иметь статичный список
// но, есть что есть, поэтому отдельным запросом вынимаем необходимые значения
export const useFullCarsList = (params: useFullCarsList) => {
  const [fullCarsList, setFullCarsList] = useState<Car[] | undefined>(undefined)
  useEffect(() => {
    axios.get(baseURL).then((res) => setFullCarsList(res.data))
  }, [])
  //Фильтр - уникальные значения Брэнд:
  const uniqueBrand = [
    ...new Set(
      fullCarsList?.map((el) => {
        return el.brand
      })
    ),
  ]
  uniqueBrand.sort((a, b) => a.localeCompare(b))
  //Фильтр - уникальные значения Модели для выбранного Брэнда:
  const ModelForSelectBrand: string[] = []
  fullCarsList?.map((el) => {
    if (el.brand === params.brandValue) {
      ModelForSelectBrand.push(el.model)
    }
  })
  const uniqueModelForSelectBrand: string[] = [...new Set(ModelForSelectBrand)]
  uniqueModelForSelectBrand.sort((a, b) => a.localeCompare(b))
  return { uniqueBrand, uniqueModelForSelectBrand }
}

/*
//SingleCar request
export const useSingleCar = () => {
  const params = useParams()
  const [car, setCar] = useState<Car | undefined>(undefined)
  const [loading, setLoading] = useState<boolean>(true)
  useEffect(() => {
    if (!loading) {
      setLoading(true)
    }
    axios
      .get(baseURL + 'id=' + params.slug)
      .then((res) => setCar(res.data[0]))
      .finally(() => setLoading(false))
  }, [params?.id])

  return { car, loading }
}
*/
