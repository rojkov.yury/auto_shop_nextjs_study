create-next-app

## Getting Started

First, run the development server:
`npm run concurrently`

Open [http://localhost:3000] with your browser to see the result.
Open [http://localhost:3001/cars] with your browser to see the JSON database.

installing dependencies:

npm i styled-components@latest
npm i i18next --save
npm i -D json-server
npm i -D concurrently
npm i -D axios
npm i react-helmet
npm i --save-dev @types/react-helmet
npm i --save-dev @types/styled-components
